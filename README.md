# Commit to Change

[![Release](https://img.shields.io/github/release/jitpack/gradle-simple.svg?label=gradle)](https://jitpack.io/#org.bitbucket.salab/commit2change)
[![Kotlin](https://img.shields.io/badge/kotlin-1.0.0--beta--2422-blue.svg)](http://kotlinlang.org)
[![DUB](https://img.shields.io/dub/l/vibe-d.svg)](https://github.com/salab/commit2change/blob/master/LICENSE)

Converts commits into changes.

One change represents one or multi-edit operations at once (the commit-time).

## Usage

# Add jitpack.io as Maven central repository.
```
repositories {
    // other repositories.  e.g. jcenter()
    maven { url "https://jitpack.io" }
}
```

# Add the dependency.

```
dependencies {
    compile "org.bitbucket.salab:commit2change:${release_version}"
    // Each name of published tags represents ${release_version}.
    // You can choose one of them and use it.
}
```