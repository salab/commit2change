package jp.ac.titech.cs.se.commit2changes.model

/**
 * Created by jmatsu on 2015/11/13.
 */
data class FilePair(val id: String, val msg: String, val older: CFile, val newer: CFile)