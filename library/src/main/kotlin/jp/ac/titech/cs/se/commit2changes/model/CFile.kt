package jp.ac.titech.cs.se.commit2changes.model

/**
 * Created by jmatsu on 2015/11/13.
 */
data class CFile(
        val path: String,
        val time: Long,
        val content: String
)