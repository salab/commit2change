package jp.ac.titech.cs.se.commit2changes

import jp.ac.titech.cs.se.commit2changes.helper.asFalse
import jp.ac.titech.cs.se.commit2changes.helper.chainIf
import jp.ac.titech.cs.se.commit2changes.helper.isNotNull
import jp.ac.titech.cs.se.commit2changes.model.CFile
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.treewalk.CanonicalTreeParser
import org.eclipse.jgit.treewalk.TreeWalk
import org.eclipse.jgit.treewalk.filter.PathFilter
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * Created by jmatsu on 2015/10/03.
 */
public class RevWalker(val repository: Repository, val refs: String): Iterator<RevWalker.Commit> {
    private val walk = RevWalk(repository)
    private val headId = repository.getRef(refs).objectId
    private var currentCommit : Commit? = Commit(walk, repository, headId)

    fun reinitialize() {
        currentCommit = Commit(walk, repository, headId)
    }

    override fun next(): Commit {
        val ret = currentCommit
        currentCommit = currentCommit?.parent
        return ret!! // Runtime error happens if invalid.
    }

    override fun hasNext(): Boolean {
        // TODO Make this be able to walk on multiple branches.
        return currentCommit?.canUpTo().asFalse()
    }

    public class Commit(val walk: RevWalk, val repository: Repository, val id: ObjectId) {
        val commit = walk.parseCommit(id)
        val parents = commit.parents
        val parent: Commit? by lazy { upTo() }

        fun upTo() =
                parents.firstOrNull()?.let { p ->
                    Commit(walk, repository, p.id)
                }

        fun canUpTo(): Boolean {
            return parents?.size?.equals(1).isNotNull() // FIXME first parent only supported now
        }

        fun changedFiles(): List<DiffEntry> {
            return if (parent?.commit?.tree != null) {
                 Git(repository).diff().apply {
                    setNewTree(CanonicalTreeParser().apply {
                        reset(repository.newObjectReader(), parent?.commit?.tree!!.id)
                    })

                    setOldTree(CanonicalTreeParser().apply {
                        reset(repository.newObjectReader(), commit.tree.id)
                    })
                }.call()
            } else {
                ArrayList()
            }
        }

        fun toCFile(path: String) = CFile(path, commit.commitTime.toLong(), readFile(path))

        fun readFile(path: String, saveTo: File): Boolean {
            return TreeWalk(repository).apply {
                addTree(commit.tree)
                isRecursive = true
                filter = PathFilter.create(path)
            }.chainIf { it.next() }?.let { s ->
                repository.run {
                    open(s.getObjectId(0))
                }.copyTo(FileOutputStream(saveTo))
            }.isNotNull()
        }

        fun readFile(path: String): String {
            return File(path).run {
                File.createTempFile(name, extension).chainIf { tf ->
                    readFile(path, tf)
                }
            }?.run {
                readLines("utf-8").joinToString("\n")
            }.orEmpty()
        }
    }
}