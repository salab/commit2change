package jp.ac.titech.cs.se.commit2changes.model

/**
 * Created by jmatsu on 2015/11/13.
 */
data class CChunk(
        val file: String,
        val offset: Int,
        val epoch: Long,
        val added: String,
        val removed: String
)