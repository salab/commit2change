package jp.ac.titech.cs.se.commit2changes.model

import java.util.*

/**
 * Created by jmatsu on 2015/10/04.
 */
data class CChange(
        val cchunks: ArrayList<CChunk>,
        val message: String
) {
    fun add(c: CChunk) {
        cchunks.add(c)
    }

    fun isEmpty(): Boolean {
        return cchunks.isEmpty()
    }
}