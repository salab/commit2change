package jp.ac.titech.cs.se.commit2changes

import com.sksamuel.diffpatch.DiffMatchPatch
import jp.ac.titech.cs.se.commit2changes.helper.chainIf
import jp.ac.titech.cs.se.commit2changes.helper.isNotNull
import jp.ac.titech.cs.se.commit2changes.model.CChange
import jp.ac.titech.cs.se.commit2changes.model.CChunk
import jp.ac.titech.cs.se.commit2changes.model.FilePair
import org.eclipse.jgit.lib.Repository
import java.util.*

/**
 * Created by jmatsu on 2015/10/03.
 */
public open class Converter(val repository: Repository, refs: String) {
    private val extractor = Extractor(repository, refs)

    fun file(): Calculator<FilePair> {
        return DefaultCalculator(this)
    }

    fun change(): Calculator<CChange> {
        return CChangeCalculator(this)
    }

    fun reinitialize() {
        extractor.walker.reinitialize()
    }

    interface Calculator<T> {
        fun converter(): Converter

        fun start(depth: Int): List<List<T>> {
            return converter().extractor.walk(depth).map { lcf ->
                calc(lcf)
            }.filterNot { it.size == 0 }
        }

        fun forEach(saver: (List<FilePair>) -> Boolean, consumer: (List<List<T>>) -> Unit) {
            var current: RevWalker.Commit
            val walker = converter().extractor.walker

            val list = ArrayList<List<FilePair>>()

            val pairs = run {
                if (!walker.hasNext()) {
                    ArrayList<FilePair>()
                }

                current = walker.next()

                if (current.parent != null) {
                    current.changedFiles().map { file ->
                        val revised = current.toCFile(file.newPath)
                        val original = current.parent!!.toCFile(file.oldPath)

                        FilePair(current.id.name, current.commit.shortMessage, original, revised)
                    }
                } else {
                    emptyList()
                }
            }

            list.add(pairs)

            if (saver(pairs)) {
                list.map {
                    calc(it)
                }.filter {
                    it.size != 0
                }.let {
                    consumer(it)
                }
            }
        }

        fun calc(lcf: List<FilePair>) : List<T>
    }

    class Extractor(repository: Repository, refs: String) {
        val walker = RevWalker(repository, refs)

        fun walk(depth: Int): List<List<FilePair>> {
            var now: RevWalker.Commit

            return 0.until(depth).map {
                if (!walker.hasNext()) {
                    return ArrayList()
                }

                now = walker.next()

                now.chainIf { now.parent.isNotNull() }?.changedFiles()?.map { e ->
                    val new = now.toCFile(e.newPath)
                    val old = now.parent!!.toCFile(e.oldPath)

                    FilePair(now.id.name, now.commit.shortMessage, old, new)
                }.orEmpty()
            }.filter { it.isNotEmpty() }
        }
    }

    private class DefaultCalculator(private val converter: Converter): Calculator<FilePair> {
        override fun calc(lcf: List<FilePair>): List<FilePair> {
            return lcf.filter { it.isNotNull() }
        }


        override fun converter(): Converter {
            return converter
        }
    }


    private class CChangeCalculator(private val converter: Converter): Calculator<CChange> {
        private val differ = DiffMatchPatch()

        override fun calc(lcf: List<FilePair>): List<CChange> {
            return lcf.map { calc_internal(it) }.filterNot { it.isEmpty() }
        }

        fun calc_internal(cf: FilePair): CChange {
            return cf.run {
                CChange(ArrayList(), msg).apply {
                    var offset = 0

                    differ.diff_main(older.content, newer.content).forEach { d ->
                        if (d.operation != DiffMatchPatch.Operation.EQUAL) {
                            val text = when (d.operation) {
                                DiffMatchPatch.Operation.INSERT -> d.text.to("")
                                DiffMatchPatch.Operation.DELETE -> "".to(d.text)
                                else -> "".to("")
                            }

                            add(CChunk(newer.path, offset, newer.time, text.first, text.second))
                        }

                        offset = offset.plus(d.text.length)
                    }
                }
            }
        }

        override fun converter(): Converter {
            return converter
        }
    }
}