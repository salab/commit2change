package jp.ac.titech.cs.se.commit2changes.helper

/**
 * Created by jmatsu on 2015/10/03.
 */
public fun Any?.isNull(): Boolean {
    return this == null
}

public fun Any?.isNotNull(): Boolean {
    return !isNull()
}

public fun Boolean?.asFalse(): Boolean {
    return if (isNull()) false else this!!
}

public fun <T: Any> T.chainIf(f: (T) -> Boolean): T? {
    return if (f.invoke(this)) {
        this
    } else {
        null
    }
}

public fun Int?.zero(): Int {
    return 0
}